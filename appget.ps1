﻿. "$psscriptroot\lib\core.ps1"
. "$root\lib\bucket.ps1"
. "$root\lib\install.ps1"

reset_aliases

$cmd = $args[0]

if ($cmd -eq 'update') {
    if ($args.Length -eq 1){
        bucket_update
    } else {
        warn "Update of app not implemented yet"
    }
 
} elseif ($cmd -eq 'install') {
    if ($args.Length -eq 1){
        error "Specify app name"
    } else {
        foreach ($app_name in $args[1..$args.Count]) {
            install_app $app_name
        }
    }

} elseif ($cmd -eq 'cache') {
    $action = $args[1]
    $app = $args[2]

    switch($action) {
        'rm' {
            if(!$app) { abort "Argument <app> missing" }
            rm "$cachedir\$app#*"
        }
        'show' {
            cache_show $app
        }
        '' {
            cache_show
        }
        default {
            Write-Host "Usage: appget cache show | rm [app]"
        }
    }

} else {
    error "Unknown command: $($args[0])"
    exit 1
}


