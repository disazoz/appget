﻿. "$psscriptroot\core.ps1"
. "$psscriptroot\bucket.ps1"

reset_aliases

function install_app($app){
    $manifest = get_manifest $app
    $version = $manifest.version
    $dir = "$apps\$app"

    info "Installing '$app' ($version)"

    ensure $apps\$app | Out-Null
    dl_files $app $version $manifest.files $temp

    if ($manifest.install) {
        info "Run install script"
        Invoke-Expression $manifest.install
    }

    foreach ($s in $manifest.shortcuts) {
		if (is_admin){
			$path = "$env:Public\Desktop\$($s[0]).lnk"
		} else {
			$path = "$env:USERPROFILE\Desktop\$($s[0]).lnk"
		}
		
		$target = $s[1]
		if(test-path "$dir\$target" -pathType leaf) {
            $bin = "$dir\$target"
        } elseif(test-path $target -pathType leaf) {
            $bin = $target
        } else {
            $bin = search_in_path $target
        }
        if(!$bin) { abort "Can't create shortcut for '$target': File doesn't exist."}
				
        create_shortcut $path $bin $s[2]
    }

    # Create shim
    foreach ($s in $manifest.shims) {
        $name = $s[0]
        $target = $s[1]
        $arg = $s[2]
        info "Creating shim for '$name'"

        if(test-path "$dir\$target" -pathType leaf) {
            $bin = "$dir\$target"
        } elseif(test-path $target -pathType leaf) {
            $bin = $target
        } else {
            $bin = search_in_path $target
        }
        if(!$bin) { abort "Can't shim '$target': File doesn't exist."}

        shim $bin $name $arg
    }

    success "'$app' ($version) was installed successfully!"
    
}



function create_shortcut($path, $target, $params){
    $WScriptShell = New-Object -ComObject WScript.Shell
    $sc = $WScriptShell.CreateShortcut($path)
	$sc.Arguments = $params
    $sc.TargetPath = $target
    $sc.Save()
}

function dl_files($app, $version, $files, $dir, $use_cache = $true, $check_hash = $false) {
    # we only want to show this warning once
    if(!$use_cache) { warn "Cache is being ignored." }

    $cookies = $null

    foreach($file in $files) {
        $url = $file.url
        $fname = url_filename $url
        
        try {
           dl_with_cache $app $version $url "$dir\$fname" $cookies $use_cache
        } catch {
           write-host -f darkred $_
           abort "URL $url is not valid"
        }

        if($check_hash) {
            $manifest_hash = hash_for_url $manifest $url $architecture
            $ok, $err = check_hash "$dir\$fname" $manifest_hash $(show_app $app $bucket)
            if(!$ok) {
                error $err
                $cached = cache_path $app $version $url
                if(test-path $cached) {
                    # rm cached file
                    Remove-Item -force $cached
                }
                if($url.Contains('sourceforge.net')) {
                    Write-Host -f yellow 'SourceForge.net is known for causing hash validation fails. Please try again before opening a ticket.'
                }
                abort $(new_issue_msg $app $bucket "hash check failed")
            }
        }
    }
    

    foreach($file in $files) {
        $url = $file.url
        $fname = url_filename $url

        if ($fname -match "\.7z$") {
            info "Extract $fname"
            Expand-7zipArchive -Path $temp\$fname -DestinationPath $apps\$app
            rm $temp\$fname -Force
        }
    }
}


function dl_with_cache($app, $version, $url, $to, $cookies = $null, $use_cache = $true) {
    $cached = fullpath (cache_path $app $version $url)
    
    if(!(test-path $cached) -or !$use_cache) {
        ensure $cachedir | Out-Null
        do_dl $url "$cached.download" $cookies
        Move-Item "$cached.download" $cached -force
    } else { info "Loading $(url_filename $url) from cache"}

    if (!($null -eq $to)) {
        Copy-Item $cached $to
    }
}

function do_dl($url, $to, $cookies) {
    $progress = [console]::isoutputredirected -eq $false -and
        $host.name -ne 'Windows PowerShell ISE Host'

    try {
       $url = handle_special_urls $url
       dl $url $to $cookies $progress
    } catch {
       $e = $_.exception
       if($e.innerexception) { $e = $e.innerexception }
       throw $e
    }
}

# download with filesize and progress indicator
function dl($url, $to, $cookies, $progress) {
    $reqUrl = ($url -split "#")[0]
    $wreq = [net.webrequest]::create($reqUrl)
    if($wreq -is [net.httpwebrequest]) {
        $wreq.useragent = Get-UserAgent
        if (-not ($url -imatch "sourceforge\.net" -or $url -imatch "portableapps\.com")) {
            $wreq.referer = strip_filename $url
        }
        if($cookies) {
            $wreq.headers.add('Cookie', (cookie_header $cookies))
        }
    }

    try {
        $wres = $wreq.GetResponse()
    } catch [System.Net.WebException] {
        $exc = $_.Exception
        $handledCodes = @(
            [System.Net.HttpStatusCode]::MovedPermanently,  # HTTP 301
            [System.Net.HttpStatusCode]::Found,             # HTTP 302
            [System.Net.HttpStatusCode]::SeeOther,          # HTTP 303
            [System.Net.HttpStatusCode]::TemporaryRedirect  # HTTP 307
        )

        # Only handle redirection codes
        $redirectRes = $exc.Response
        if ($handledCodes -notcontains $redirectRes.StatusCode) {
            throw $exc
        }

        # Get the new location of the file
        if ((-not $redirectRes.Headers) -or ($redirectRes.Headers -notcontains 'Location')) {
            throw $exc
        }

        $newUrl = $redirectRes.Headers['Location']
        info "Following redirect to $newUrl..."

        # Handle manual file rename
        if ($url -like '*#/*') {
            $null, $postfix = $url -split '#/'
            $newUrl = "$newUrl#/$postfix"
        }

        dl $newUrl $to $cookies $progress
        return
    }

    $total = $wres.ContentLength
    if($total -eq -1 -and $wreq -is [net.ftpwebrequest]) {
        $total = ftp_file_size($url)
    }

    if ($progress -and ($total -gt 0)) {
        [console]::CursorVisible = $false
        function dl_onProgress($read) {
            dl_progress $read $total $url
        }
    } else {
        write-host "Downloading $url ($(filesize $total))..."
        function dl_onProgress {
            #no op
        }
    }

    try {
        $s = $wres.getresponsestream()
        $fs = [io.file]::openwrite($to)
        $buffer = new-object byte[] 2048
        $totalRead = 0
        $sw = [diagnostics.stopwatch]::StartNew()

        dl_onProgress $totalRead
        while(($read = $s.read($buffer, 0, $buffer.length)) -gt 0) {
            $fs.write($buffer, 0, $read)
            $totalRead += $read
            if ($sw.elapsedmilliseconds -gt 100) {
                $sw.restart()
                dl_onProgress $totalRead
            }
        }
        $sw.stop()
        dl_onProgress $totalRead
    } finally {
        if ($progress) {
            [console]::CursorVisible = $true
            write-host
        }
        if ($fs) {
            $fs.close()
        }
        if ($s) {
            $s.close();
        }
        $wres.close()
    }
}

function dl_progress_output($url, $read, $total, $console) {
    $filename = url_filename $url

    # calculate current percentage done
    $p = [math]::Round($read / $total * 100, 0)

    # pre-generate LHS and RHS of progress string
    # so we know how much space we have
    $left  = "$filename ($(filesize $total))"
    $right = [string]::Format("{0,3}%", $p)

    # calculate remaining width for progress bar
    $midwidth  = $console.BufferSize.Width - ($left.Length + $right.Length + 8)

    # calculate how many characters are completed
    $completed = [math]::Abs([math]::Round(($p / 100) * $midwidth, 0) - 1)

    # generate dashes to symbolise completed
    if ($completed -gt 1) {
        $dashes = [string]::Join("", ((1..$completed) | ForEach-Object {"="}))
    }

    # this is why we calculate $completed - 1 above
    $dashes += switch($p) {
        100 {"="}
        default {">"}
    }

    # the remaining characters are filled with spaces
    $spaces = switch($dashes.Length) {
        $midwidth {[string]::Empty}
        default {
            [string]::Join("", ((1..($midwidth - $dashes.Length)) | ForEach-Object {" "}))
        }
    }

    "$left [$dashes$spaces] $right"
}

function dl_progress($read, $total, $url) {
    $console = $host.UI.RawUI;
    $left  = $console.CursorPosition.X;
    $top   = $console.CursorPosition.Y;
    $width = $console.BufferSize.Width;

    if($read -eq 0) {
        $maxOutputLength = $(dl_progress_output $url 100 $total $console).length
        if (($left + $maxOutputLength) -gt $width) {
            # not enough room to print progress on this line
            # print on new line
            write-host
            $left = 0
            $top  = $top + 1
            if($top -gt $console.CursorPosition.Y) { $top = $console.CursorPosition.Y }
        }
    }

    write-host $(dl_progress_output $url $read $total $console) -nonewline
    [console]::SetCursorPosition($left, $top)
}

function url_filename($url) {
    (split-path $url -leaf).split('?') | Select-Object -First 1
}

# hashes
function hash_for_url($manifest, $url, $arch) {
    $hashes = @(hash $manifest $arch) | Where-Object { $_ -ne $null };

    if($hashes.length -eq 0) { return $null }

    $urls = @(script:url $manifest $arch)

    $index = [array]::indexof($urls, $url)
    if($index -eq -1) { abort "Couldn't find hash in manifest for '$url'." }

    @($hashes)[$index]
}

# returns (ok, err)
function check_hash($file, $hash, $app_name) {
    $file = fullpath $file
    if(!$hash) {
        warn "Warning: No hash in manifest. SHA256 for '$(fname $file)' is:`n    $(compute_hash $file 'sha256')"
        return $true, $null
    }

    Write-Host "Checking hash of " -NoNewline
    Write-Host $(url_remote_filename $url) -f Cyan -NoNewline
    Write-Host " ... " -nonewline
    $algorithm, $expected = get_hash $hash
    if ($null -eq $algorithm) {
        return $false, "Hash type '$algorithm' isn't supported."
    }

    $actual = compute_hash $file $algorithm
    $expected = $expected.ToLower()

    if($actual -ne $expected) {
        $msg = "Hash check failed!`n"
        $msg += "App:         $app_name`n"
        $msg += "URL:         $url`n"
        if(Test-Path $file) {
            $msg += "First bytes: $((get_magic_bytes_pretty $file ' ').ToUpper())`n"
        }
        if($expected -or $actual) {
            $msg += "Expected:    $expected`n"
            $msg += "Actual:      $actual"
        }
        return $false, $msg
    }
    Write-Host "ok." -f Green
    return $true, $null
}

function compute_hash($file, $algname) {
    try {
        if(Test-CommandAvailable Get-FileHash) {
            return (Get-FileHash -Path $file -Algorithm $algname).Hash.ToLower()
        } else {
            $fs = [system.io.file]::openread($file)
            $alg = [system.security.cryptography.hashalgorithm]::create($algname)
            $hexbytes = $alg.computehash($fs) | ForEach-Object { $_.tostring('x2') }
            return [string]::join('', $hexbytes)
        }
    } catch {
        error $_.exception.message
    } finally {
        if($fs) { $fs.dispose() }
        if($alg) { $alg.dispose() }
    }
    return ''
}

function handle_special_urls($url)
{
    # FossHub.com
    if ($url -match "^(?:.*fosshub.com\/)(?<name>.*)(?:\/|\?dwl=)(?<filename>.*)$") {
        $Body = @{
            projectUri      = $Matches.name;
            fileName        = $Matches.filename;
            source          = 'CF';
            isLatestVersion = $true
        }
        if ((Invoke-RestMethod -Uri $url) -match '"p":"(?<pid>[a-f0-9]{24}).*?"r":"(?<rid>[a-f0-9]{24})') {
            $Body.Add("projectId", $Matches.pid)
            $Body.Add("releaseId", $Matches.rid)
        }
        $url = Invoke-RestMethod -Method Post -Uri "https://api.fosshub.com/download/" -ContentType "application/json" -Body (ConvertTo-Json $Body -Compress)
        if ($null -eq $url.error) {
            $url = $url.data.url
        }
    }

    # Sourceforge.net
    if ($url -match "(?:downloads\.)?sourceforge.net\/projects?\/(?<project>[^\/]+)\/(?:files\/)?(?<file>.*?)(?:$|\/download|\?)") {
        # Reshapes the URL to avoid redirections
        $url = "https://downloads.sourceforge.net/project/$($matches['project'])/$($matches['file'])"
    }
    return $url
}

function Get-UserAgent() {
    return "AppGet/1.0 PowerShell/$($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) (Windows NT $([System.Environment]::OSVersion.Version.Major).$([System.Environment]::OSVersion.Version.Minor); $(if($env:PROCESSOR_ARCHITECTURE -eq 'AMD64'){'Win64; x64; '})$(if($env:PROCESSOR_ARCHITEW6432 -eq 'AMD64'){'WOW64; '})$PSEdition)"
}


function shim($path, $name, $arg) {
    if (!(Test-Path $path)) { abort "Can't shim '$(fname $path)': couldn't find '$path'." }
    $abs_shimdir = ensure $shimsdir
    if (!$name) { $name = strip_ext (fname $path) }

    $shim = "$abs_shimdir\$($name.tolower())"

    # convert to relative path
    Push-Location $abs_shimdir
    $relative_path = Resolve-Path -Relative $path
    Pop-Location
    $resolved_path = Resolve-Path $path

    if ($path -match '\.(exe|com)$') {
        # for programs with no awareness of any shell
        warn_on_overwrite "$shim.shim" $path
        Copy-Item $shimexe "$shim.exe" -Force
         
        Write-Output "path = $resolved_path" | Out-File "$shim.shim" -Encoding ASCII
        if ($arg) {
            Write-Output "args = $arg" | Out-File "$shim.shim" -Encoding ASCII -Append
        }
    } elseif ($path -match '\.(bat|cmd)$') {
        # shim .bat, .cmd so they can be used by programs with no awareness of PSH
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@`"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
MSYS2_ARG_CONV_EXCL=/C cmd.exe /C `"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    } elseif ($path -match '\.ps1$') {
        # if $path points to another drive resolve-path prepends .\ which could break shims
        warn_on_overwrite "$shim.ps1" $path
        $ps1text = if ($relative_path -match '^(.\\[\w]:).*$') {
            "# $resolved_path
`$path = `"$path`"
if(`$myinvocation.expectingInput) { `$input | & `$path $arg @args } else { & `$path $arg @args }
exit `$lastexitcode"
        } else {
            # Setting PSScriptRoot in Shim if it is not defined, so the shim doesn't break in PowerShell 2.0
            "# $resolved_path
if (!(Test-Path Variable:PSScriptRoot)) { `$PSScriptRoot = Split-Path `$MyInvocation.MyCommand.Path -Parent }
`$path = join-path `"`$psscriptroot`" `"$relative_path`"
if(`$myinvocation.expectingInput) { `$input | & `$path $arg @args } else { & `$path $arg @args }
exit `$lastexitcode"
        }
        $ps1text | Out-File "$shim.ps1" -Encoding ASCII

        # make ps1 accessible from cmd.exe
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@echo off
setlocal enabledelayedexpansion
set args=%*
:: replace problem characters in arguments
set args=%args:`"='%
set args=%args:(=``(%
set args=%args:)=``)%
set invalid=`"='
if !args! == !invalid! ( set args= )
where /q pwsh.exe
if %errorlevel% equ 0 (
    pwsh -noprofile -ex unrestricted -command `"& '$resolved_path' $arg %args%;exit `$lastexitcode`"
) else (
    powershell -noprofile -ex unrestricted -command `"& '$resolved_path' $arg %args%;exit `$lastexitcode`"
)" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
if command -v pwsh.exe &> /dev/null; then
    pwsh.exe -noprofile -ex unrestricted -command `"& '$resolved_path' $arg $@;exit \`$lastexitcode`"
else
    powershell.exe -noprofile -ex unrestricted -command `"& '$resolved_path' $arg $@;exit \`$lastexitcode`"
fi" | Out-File $shim -Encoding ASCII
    } elseif ($path -match '\.jar$') {
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@java -jar `"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
java -jar `"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    } elseif ($path -match '\.py$') {
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@python `"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
python `"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    } else {
        warn_on_overwrite "$shim.cmd" $path
        # find path to Git's bash so that batch scripts can run bash scripts
        $gitdir = (Get-Item (Get-Command git -ErrorAction:Stop).Source -ErrorAction:Stop).Directory.Parent
        if ($gitdir.FullName -imatch 'mingw') {
            $gitdir = $gitdir.Parent
        }
        "@rem $resolved_path
@`"$(Join-Path (Join-Path $gitdir.FullName 'bin') 'bash.exe')`" `"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
`"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    }
}

