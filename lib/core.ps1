$root = Resolve-Path "$PSScriptRoot\.."
$bucket = "$root\bucket"
$manifests = "$bucket\manifests"
$temp = "$root\temp"
$apps = "$root\apps"
$cachedir = "$temp\cache"
$shimsdir = "$root\shims"

$7zPath = "$psscriptroot\7za.exe"
$shimexe = "$psscriptroot\shim.exe"


function installed($app) {
    $app = $app.split("/")[-1]
    return is_directory $apps\$app
}

function is_directory([String] $path) {
    return (Test-Path $path) -and (Get-Item $path) -is [System.IO.DirectoryInfo]
}

# helper functions
function coalesce($a, $b) { if($a) { return $a } $b }

function ensure($dir) { if(!(test-path $dir)) { mkdir $dir > $null }; resolve-path $dir }

function format($str, $hash) {
    $hash.keys | ForEach-Object { set-variable $_ $hash[$_] }
    $executionContext.invokeCommand.expandString($str)
}
function is_admin {
    $admin = [security.principal.windowsbuiltinrole]::administrator
    $id = [security.principal.windowsidentity]::getcurrent()
    ([security.principal.windowsprincipal]($id)).isinrole($admin)
}

function pluralize($count, $singular, $plural) {
    if($count -eq 1) { $singular } else { $plural }
}

# paths
function fname($path) { split-path $path -leaf }
function strip_ext($fname) { $fname -replace '\.[^\.]*$', '' }
function strip_filename($path) { $path -replace [regex]::escape((fname $path)) }
function strip_fragment($url) { $url -replace (new-object uri $url).fragment }
function cache_path($app, $version, $url) { "$cachedir\$app#$version#$($url -replace '[^\w\.\-]+', '_')" }
function fullpath($path) { $executionContext.sessionState.path.getUnresolvedProviderPathFromPSPath($path) }

# messages
function abort($msg, [int] $exit_code=1) { write-host "ABORT $msg" -f red; exit $exit_code }
function error($msg) {   write-host "ERROR $msg" -f darkred }
function warn($msg) {    write-host "WARN  $msg" -f darkyellow }
function info($msg) {    write-host "INFO  $msg" -f darkgray }
function success($msg) { write-host "SUCCESS $msg" -f darkgreen }
function debug($obj) {
    if((get_config 'debug' $false) -ine 'true' -and $env:SCOOP_DEBUG -ine 'true') {
        return
    }

    $prefix = "DEBUG[$(Get-Date -UFormat %s)]"
    $param = $MyInvocation.Line.Replace($MyInvocation.InvocationName, '').Trim()
    $msg = $obj | Out-String -Stream

    if($null -eq $obj -or $null -eq $msg) {
        Write-Host "$prefix $param = " -f DarkCyan -NoNewline
        Write-Host '$null' -f DarkYellow -NoNewline
        Write-Host " -> $($MyInvocation.PSCommandPath):$($MyInvocation.ScriptLineNumber):$($MyInvocation.OffsetInLine)" -f DarkGray
        return
    }

    if($msg.GetType() -eq [System.Object[]]) {
        Write-Host "$prefix $param ($($obj.GetType()))" -f DarkCyan -NoNewline
        Write-Host " -> $($MyInvocation.PSCommandPath):$($MyInvocation.ScriptLineNumber):$($MyInvocation.OffsetInLine)" -f DarkGray
        $msg | Where-Object { ![String]::IsNullOrWhiteSpace($_) } |
            Select-Object -Skip 2 | # Skip headers
            ForEach-Object {
                Write-Host "$prefix $param.$($_)" -f DarkCyan
            }
    } else {
        Write-Host "$prefix $param = $($msg.Trim())" -f DarkCyan -NoNewline
        Write-Host " -> $($MyInvocation.PSCommandPath):$($MyInvocation.ScriptLineNumber):$($MyInvocation.OffsetInLine)" -f DarkGray
    }
}


function env($name,$machine,$val='__get') {
    $target = 'User'; if($machine) {$target = 'Machine'}
    if($val -eq '__get') { [environment]::getEnvironmentVariable($name,$target) }
    else { [environment]::setEnvironmentVariable($name,$val,$target) }
}


function ensure_in_path($dir, $machine) {
    $path = env 'PATH' $machine
    $dir = fullpath $dir
    if($path -notmatch [regex]::escape($dir)) {
        info "Adding $dir to $(if($machine){'machine'}else{'user'}) path."

        env 'PATH' $machine "$dir;$path" # for future sessions...
        $env:PATH = "$dir;$env:PATH" # for this session
    }
}


function ensure_scoop_in_path($machine) {
    ensure_in_path $shimsdir $machine
}

function filesize($length) {
    $gb = [math]::pow(2, 30)
    $mb = [math]::pow(2, 20)
    $kb = [math]::pow(2, 10)

    if($length -gt $gb) {
        "{0:n1} GB" -f ($length / $gb)
    } elseif($length -gt $mb) {
        "{0:n1} MB" -f ($length / $mb)
    } elseif($length -gt $kb) {
        "{0:n1} KB" -f ($length / $kb)
    } else {
        "$($length) B"
    }
}

function reset_alias($name, $value) {
    if($existing = get-alias $name -ea ignore | Where-Object { $_.options -match 'readonly' }) {
        if($existing.definition -ne $value) {
            write-host "Alias $name is read-only; can't reset it." -f darkyellow
        }
        return # already set
    }
    if($value -is [scriptblock]) {
        if(!(test-path -path "function:script:$name")) {
            new-item -path function: -name "script:$name" -value $value | out-null
        }
        return
    }

    set-alias $name $value -scope script -option allscope
}

function reset_aliases() {
    # for aliases where there's a local function, re-alias so the function takes precedence
    $aliases = get-alias | Where-Object { $_.options -notmatch 'readonly|allscope' } | ForEach-Object { $_.name }
    get-childitem function: | ForEach-Object {
        $fn = $_.name
        if($aliases -contains $fn) {
            set-alias $fn local:$fn -scope script
        }
    }

    # for dealing with user aliases
    $default_aliases = @{
        'cp' = 'copy-item'
        'echo' = 'write-output'
        'gc' = 'get-content'
        'gci' = 'get-childitem'
        'gcm' = 'get-command'
        'gm' = 'get-member'
        'iex' = 'invoke-expression'
        'ls' = 'get-childitem'
        'mkdir' = { new-item -type directory @args }
        'mv' = 'move-item'
        'rm' = 'remove-item'
        'sc' = 'set-content'
        'select' = 'select-object'
        'sls' = 'select-string'
    }

    # set default aliases
    $default_aliases.keys | ForEach-Object { reset_alias $_ $default_aliases[$_] }
}

function run($exe, $arg, $msg, $continue_exit_codes) {
    Show-DeprecatedWarning $MyInvocation 'Invoke-ExternalCommand'
    Invoke-ExternalCommand -FilePath $exe -ArgumentList $arg -Activity $msg -ContinueExitCodes $continue_exit_codes
}

function Invoke-ExternalCommand {
    [CmdletBinding(DefaultParameterSetName = "Default")]
    [OutputType([Boolean])]
    param (
        [Parameter(Mandatory = $true,
                   Position = 0)]
        [Alias("Path")]
        [ValidateNotNullOrEmpty()]
        [String]
        $FilePath,
        [Parameter(Position = 1)]
        [Alias("Args")]
        [String[]]
        $ArgumentList,
        [Parameter(ParameterSetName = "UseShellExecute")]
        [Switch]
        $RunAs,
        [Alias("Msg")]
        [String]
        $Activity,
        [Alias("cec")]
        [Hashtable]
        $ContinueExitCodes,
        [Parameter(ParameterSetName = "Default")]
        [Alias("Log")]
        [String]
        $LogPath
    )
    if ($Activity) {
        Write-Host "$Activity " -NoNewline
    }
    $Process = New-Object System.Diagnostics.Process
    $Process.StartInfo.FileName = $FilePath
    $Process.StartInfo.Arguments = ($ArgumentList | Select-Object -Unique) -join ' '
    $Process.StartInfo.UseShellExecute = $false
    if ($LogPath) {
        if ($FilePath -match '(^|\W)msiexec($|\W)') {
            $Process.StartInfo.Arguments += " /lwe `"$LogPath`""
        } else {
            $Process.StartInfo.RedirectStandardOutput = $true
            $Process.StartInfo.RedirectStandardError = $true
        }
    }
    if ($RunAs) {
        $Process.StartInfo.UseShellExecute = $true
        $Process.StartInfo.Verb = 'RunAs'
    }
    try {
        $Process.Start() | Out-Null
    } catch {
        if ($Activity) {
            Write-Host "error." -ForegroundColor DarkRed
        }
        error $_.Exception.Message
        return $false
    }
    if ($LogPath -and ($FilePath -notmatch '(^|\W)msiexec($|\W)')) {
        Out-File -FilePath $LogPath -Encoding Default -Append -InputObject $Process.StandardOutput.ReadToEnd()
        Out-File -FilePath $LogPath -Encoding Default -Append -InputObject $Process.StandardError.ReadToEnd()
    }
    $Process.WaitForExit()
    if ($Process.ExitCode -ne 0) {
        if ($ContinueExitCodes -and ($ContinueExitCodes.ContainsKey($Process.ExitCode))) {
            if ($Activity) {
                Write-Host "done." -ForegroundColor DarkYellow
            }
            warn $ContinueExitCodes[$Process.ExitCode]
            return $true
        } else {
            if ($Activity) {
                Write-Host "error." -ForegroundColor DarkRed
            }
            error "Exit code was $($Process.ExitCode)!"
            return $false
        }
    }
    if ($Activity) {
        Write-Host "done." -ForegroundColor Green
    }
    return $true
}

function Expand-7zipArchive {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [String]
        $Path,
        [Parameter(Position = 1)]
        [String]
        $DestinationPath = (Split-Path $Path),
        [String]
        $ExtractDir,
        [Parameter(ValueFromRemainingArguments = $true)]
        [String]
        $Switches,
        [ValidateSet("All", "Skip", "Rename")]
        [String]
        $Overwrite,
        [Switch]
        $Removal
    )

    $LogPath = "$(Split-Path $Path)\7zip.log"
    $ArgList = @('x', "`"$Path`"", "-o`"$DestinationPath`"", '-y')
    $IsTar = ((strip_ext $Path) -match '\.tar$') -or ($Path -match '\.t[abgpx]z2?$')
    if (!$IsTar -and $ExtractDir) {
        $ArgList += "-ir!`"$ExtractDir\*`""
    }
    if ($Switches) {
        $ArgList += (-split $Switches)
    }
    switch ($Overwrite) {
        "All" { $ArgList += "-aoa" }
        "Skip" { $ArgList += "-aos" }
        "Rename" { $ArgList += "-aou" }
    }
    $Status = Invoke-ExternalCommand $7zPath $ArgList -LogPath $LogPath
    if (!$Status) {
        abort "Failed to extract files from $Path.`nLog file:`n $LogPath`n$(new_issue_msg $app $bucket 'decompress error')"
    }
    if (!$IsTar -and $ExtractDir) {
        movedir "$DestinationPath\$ExtractDir" $DestinationPath | Out-Null
    }
    if (Test-Path $LogPath) {
        Remove-Item $LogPath -Force
    }
    if ($IsTar) {
        # Check for tar
        $Status = Invoke-ExternalCommand $7zPath @('l', "`"$Path`"") -LogPath $LogPath
        if ($Status) {
            $TarFile = (Get-Content -Path $LogPath)[-5] -replace '.{53}(.*)', '$1' # get inner tar file name
            Expand-7zipArchive -Path "$DestinationPath\$TarFile" -DestinationPath $DestinationPath -ExtractDir $ExtractDir -Removal
        } else {
            abort "Failed to list files in $Path.`nNot a 7-Zip supported archive file."
        }
    }
    if ($Removal) {
        # Remove original archive file
        Remove-Item $Path -Force
    }
}


function cache_info($file) {
    $app, $version, $url = $file.name -split '#'
    $size = filesize $file.length
    return new-object psobject -prop @{ app=$app; version=$version; url=$url; size=$size }
}

function cache_show($app) {
    $files = @(Get-ChildItem "$cachedir" | Where-Object { $_.name -match "^$app" })
    $total_length = ($files | Measure-Object length -sum).sum -as [double]

    $f_app  = @{ expression={"$($_.app) ($($_.version))" }}
    $f_url  = @{ expression={$_.url};alignment='right'}
    $f_size = @{ expression={$_.size}; alignment='right'}


    $files | ForEach-Object { cache_info $_ } | Format-Table $f_size, $f_app, $f_url -auto -hide

    "Total: $($files.length) $(pluralize $files.length 'file' 'files'), $(filesize $total_length)"
}

function search_in_path($target) {
    $path = (env 'PATH' $false) + ";" + (env 'PATH' $true)
    foreach($dir in $path.split(';')) {
        if(test-path "$dir\$target" -pathType leaf) {
            return "$dir\$target"
        }
    }
}

function warn_on_overwrite($shim, $path) {
    if (!(Test-Path($shim))) {
        return
    }
    $shim_app = get_app_name_from_shim $shim
    $path_app = get_app_name $path
    if ($shim_app -eq $path_app) {
        return
    }
    $shimname = (fname $shim) -replace '\.shim$', '.exe'
    $filename = (fname $path) -replace '\.shim$', '.exe'
    warn "Overwriting shim ('$shimname' -> '$filename') installed from $shim_app"
}

function get_app_name($path) {
    if ($path -match '([^/\\]+)[/\\]current[/\\]') {
        return $matches[1].tolower()
    }
    return ''
}

function get_app_name_from_shim($shim) {
    if (!(Test-Path($shim))) {
        return ''
    }
    $content = (Get-Content $shim -Encoding UTF8) -join ' '
    return get_app_name $content
}

function Get-UserAgent() {
    return "AppGet/1.0 PowerShell/$($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) (Windows NT $([System.Environment]::OSVersion.Version.Major).$([System.Environment]::OSVersion.Version.Minor); $(if($env:PROCESSOR_ARCHITECTURE -eq 'AMD64'){'Win64; x64; '})$(if($env:PROCESSOR_ARCHITEW6432 -eq 'AMD64'){'WOW64; '})$PSEdition)"
}


function dl($url,$to) {
    $wc = New-Object Net.Webclient
    $wc.headers.add('Referer', (strip_filename $url))
    $wc.Headers.Add('User-Agent', (Get-UserAgent))
    $wc.downloadFile($url,$to)
}

function shim($path, $name, $arg) {
    if (!(Test-Path $path)) { abort "Can't shim '$(fname $path)': couldn't find '$path'." }
    $abs_shimdir = ensure $shimsdir
    if (!$name) { $name = strip_ext (fname $path) }

    $shim = "$abs_shimdir\$($name.tolower())"

    # convert to relative path
    Push-Location $abs_shimdir
    $relative_path = Resolve-Path -Relative $path
    Pop-Location
    $resolved_path = Resolve-Path $path

    if ($path -match '\.(exe|com)$') {
        # for programs with no awareness of any shell
        warn_on_overwrite "$shim.shim" $path
        Copy-Item $shimexe "$shim.exe" -Force
        info "$shim.exe"
        
        Write-Output "path = $resolved_path" | Out-File "$shim.shim" -Encoding ASCII
        if ($arg) {
            Write-Output "args = $arg" | Out-File "$shim.shim" -Encoding ASCII -Append
        }
    } elseif ($path -match '\.(bat|cmd)$') {
        # shim .bat, .cmd so they can be used by programs with no awareness of PSH
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@`"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
MSYS2_ARG_CONV_EXCL=/C cmd.exe /C `"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    } elseif ($path -match '\.ps1$') {
        # if $path points to another drive resolve-path prepends .\ which could break shims
        warn_on_overwrite "$shim.ps1" $path
        $ps1text = if ($relative_path -match '^(.\\[\w]:).*$') {
            "# $resolved_path
`$path = `"$path`"
if(`$myinvocation.expectingInput) { `$input | & `$path $arg @args } else { & `$path $arg @args }
exit `$lastexitcode"
        } else {
            # Setting PSScriptRoot in Shim if it is not defined, so the shim doesn't break in PowerShell 2.0
            "# $resolved_path
if (!(Test-Path Variable:PSScriptRoot)) { `$PSScriptRoot = Split-Path `$MyInvocation.MyCommand.Path -Parent }
`$path = join-path `"`$psscriptroot`" `"$relative_path`"
if(`$myinvocation.expectingInput) { `$input | & `$path $arg @args } else { & `$path $arg @args }
exit `$lastexitcode"
        }
        $ps1text | Out-File "$shim.ps1" -Encoding ASCII

        # make ps1 accessible from cmd.exe
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@echo off
setlocal enabledelayedexpansion
set args=%*
:: replace problem characters in arguments
set args=%args:`"='%
set args=%args:(=``(%
set args=%args:)=``)%
set invalid=`"='
if !args! == !invalid! ( set args= )
where /q pwsh.exe
if %errorlevel% equ 0 (
    pwsh -noprofile -ex unrestricted -command `"& '$resolved_path' $arg %args%;exit `$lastexitcode`"
) else (
    powershell -noprofile -ex unrestricted -command `"& '$resolved_path' $arg %args%;exit `$lastexitcode`"
)" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
if command -v pwsh.exe &> /dev/null; then
    pwsh.exe -noprofile -ex unrestricted -command `"& '$resolved_path' $arg $@;exit \`$lastexitcode`"
else
    powershell.exe -noprofile -ex unrestricted -command `"& '$resolved_path' $arg $@;exit \`$lastexitcode`"
fi" | Out-File $shim -Encoding ASCII
    } elseif ($path -match '\.jar$') {
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@java -jar `"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
java -jar `"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    } elseif ($path -match '\.py$') {
        warn_on_overwrite "$shim.cmd" $path
        "@rem $resolved_path
@python `"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
python `"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    } else {
        warn_on_overwrite "$shim.cmd" $path
        # find path to Git's bash so that batch scripts can run bash scripts
        $gitdir = (Get-Item (Get-Command git -ErrorAction:Stop).Source -ErrorAction:Stop).Directory.Parent
        if ($gitdir.FullName -imatch 'mingw') {
            $gitdir = $gitdir.Parent
        }
        "@rem $resolved_path
@`"$(Join-Path (Join-Path $gitdir.FullName 'bin') 'bash.exe')`" `"$resolved_path`" $arg %*" | Out-File "$shim.cmd" -Encoding ASCII

        warn_on_overwrite $shim $path
        "#!/bin/sh
# $resolved_path
`"$resolved_path`" $arg `"$@`"" | Out-File $shim -Encoding ASCII
    }
}

