. "$psscriptroot\core.ps1"

Import-Module $root\lib\powershell-yaml

$git_askpass = "$root\lib\git_askpass_env.exe" -replace "\\","/"
$env:GIT_ASKPASS = $git_askpass

reset_aliases

function bucket_ensure_exists(){
    if (!(Test-Path "$bucket\.git")) {
        rm $bucket -Recurse -ErrorAction SilentlyContinue
        mkdir $bucket | Out-Null
        pushd $bucket
        git init -b main | Out-Null
        git remote add origin https://gitlab.com/qweasd-su/software/appget/csweb.git
        git config credential.https://gitlab.com.username $env:GIT_USERNAME
        git -c credential.helper= fetch 2> $null
        popd
    }
}

function bucket_update(){
    bucket_ensure_exists

    pushd $bucket
    git config credential.https://gitlab.com.username $env:GIT_USERNAME
    git -c credential.helper= fetch
    git reset origin/main --hard
    popd
}

function get_manifest($app_name){
    $manifest_file = "$manifests\$app_name.yaml"

    if (!(Test-Path $manifest_file)) {
        abort "Couldn't find manifest for '$app' at $manifest_file"
    }

    $manifest = Get-Content $manifest_file | ConvertFrom-Yaml
    return $manifest
}


