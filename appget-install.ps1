#Requires -Version 5

$old_erroractionpreference = $erroractionpreference
$erroractionpreference = 'stop' # quit if anything goes wrong

if (($PSVersionTable.PSVersion.Major) -lt 5) {
    Write-Output "PowerShell 5 or later is required to run appget."
    Write-Output "Upgrade PowerShell: https://docs.microsoft.com/en-us/powershell/scripting/setup/installing-windows-powershell"
    break
}

# show notification to change execution policy:
$allowedExecutionPolicy = @('Unrestricted', 'RemoteSigned', 'ByPass')
if ((Get-ExecutionPolicy).ToString() -notin $allowedExecutionPolicy) {
    Write-Output "PowerShell requires an execution policy in [$($allowedExecutionPolicy -join ", ")] to run AppGet."
    Write-Output "For example, to set the execution policy to 'RemoteSigned' please run :"
    Write-Output "'Set-ExecutionPolicy RemoteSigned -scope CurrentUser'"
    break
}

$admin = [security.principal.windowsbuiltinrole]::administrator
$id = [security.principal.windowsidentity]::getcurrent()
if (!([security.principal.windowsprincipal]($id)).isinrole($admin)){
    Write-Output "Administrator privileges required to install AppGet"
    break
}

if ([System.Enum]::GetNames([System.Net.SecurityProtocolType]) -notcontains 'Tls12') {
    Write-Output "AppGet requires at least .NET Framework 4.5"
    Write-Output "Please download and install it first:"
    Write-Output "https://www.microsoft.com/net/download"
    break
}

if (Test-Path C:\AppGet) {
    write-host "AppGet is already installed. Run 'appget update' to get the latest version." -f red
    # don't abort if invoked with iex that would close the PS session
    if ($myinvocation.mycommand.commandtype -eq 'Script') { return } else { exit 1 }
}

$dir = mkdir "C:\AppGet"

# download scoop zip
$zipurl = 'https://gitlab.com/disazoz/appget/-/archive/main/appget-main.zip'
$zipfile = "$dir\appget.zip"
Write-Output 'Downloading appget...'
$wc = New-Object Net.Webclient
$wc.downloadFile($zipurl,$zipfile)

Write-Output 'Extracting...'
Add-Type -Assembly "System.IO.Compression.FileSystem"
[IO.Compression.ZipFile]::ExtractToDirectory($zipfile, "$dir\_tmp")
Copy-Item "$dir\_tmp\*main\*" $dir -Recurse -Force
Remove-Item "$dir\_tmp", $zipfile -Recurse -Force


Write-Output 'Creating shim...'
. "$dir\lib\core.ps1"
shim "$dir\appget.ps1"
ensure_scoop_in_path $true
success 'AppGet was installed successfully!'

#Write-Output "Type 'appget help' for instructions."

$erroractionpreference = $old_erroractionpreference # Reset $erroractionpreference to original value